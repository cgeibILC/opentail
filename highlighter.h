#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include "highlight.h"

class Highlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    Highlighter(QTextCharFormat normalTextFormat, QTextDocument *document, Highlight * highlightControllerGiven);
    void selectGroup(QString newGroupName = "");

signals:
    void foundString(QString);

protected:
    void highlightBlock(const QString &text);

private:
    Highlight * highlightController;
    QTextCharFormat normalFormat;
    QString groupName;
};

#endif // HIGHLIGHTER_H
