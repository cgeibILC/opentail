#ifndef FILELIST_H
#define FILELIST_H

#include <QListWidget>
#include <QListWidgetItem>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QUrl>

class FileList : public QListWidget
{
    Q_OBJECT
public:
    explicit FileList(QWidget *parent = 0);
    QString selectFile(const QString);

signals:
    void openFile(QString);

protected:
    void dropEvent(QDropEvent *event);
    void dragEnterEvent ( QDragEnterEvent * event );
    void dragMoveEvent(QDragMoveEvent *event);
signals:

public slots:

};

#endif // FILELIST_H
