#include "mainwindow.h"
#include <QApplication>
#include <QtGui>
#include <QTranslator>


int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(Icons);
    Q_INIT_RESOURCE(TextFiles);

    QTranslator translator;
    QCoreApplication::setOrganizationName("opentail");
    QCoreApplication::setOrganizationDomain("www.opentail.org");
    QCoreApplication::setApplicationName("opentail");
    QApplication::setQuitOnLastWindowClosed(false);

    QApplication a(argc, argv);

    if (!translator.load("qt_"+QLocale::system().name(),
                         QLibraryInfo::location(QLibraryInfo::TranslationsPath))) {

        qDebug("Error system loading translation");
    }
    a.installTranslator(&translator);

    if (!translator.load("opentail_" + QLocale::system().name(),
                         ":/translations")){
        qDebug("Error loading translation");
    }
    a.installTranslator(&translator);
    if (!QSystemTrayIcon::isSystemTrayAvailable()) {
        QMessageBox::critical(0, QObject::tr("Systray"),
                              QObject::tr("I couldn't detect any system tray "
                                          "on this system."));
        return 1;
    }

    MainWindow w;
    w.show();

    return a.exec();
}
