#ifndef HIGHLIGHTPARAMETERS_H
#define HIGHLIGHTPARAMETERS_H


struct HighlightParameters {
    QColor fontColor, backgroundColor;
    QString searchFor;
    bool bold,tray,mail,ignoreCase, wholeLine;
};


#endif // HIGHLIGHTPARAMETERS_H
