#include "newhighlight.h"
#include "ui_newhighlight.h"

NewHighlight::NewHighlight(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewHighlight)
{
    ui->setupUi(this);
}

NewHighlight::~NewHighlight()
{
    delete ui;
}

QString NewHighlight::getHighlightText()
{
    return ui->searchFor->text();
}

void NewHighlight::setType(int type)
{
    switch(type){
    case 0:
        break;
    case 1:
        setWindowTitle(tr("Highlight group name"));
        ui->label->setText(tr("Group name"));
        break;
    }
}
