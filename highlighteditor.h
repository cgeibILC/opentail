#ifndef HIGHLIGHTEDITOR_H
#define HIGHLIGHTEDITOR_H

#include <QDialog>
#include <QColorDialog>
#include <QStandardItemModel>
#include <QSettings>
#include <QMessageBox>
#include "highlight.h"
#include "newhighlight.h"

namespace Ui {
class HighlightEditor;
}

class HighlightEditor : public QDialog
{
    Q_OBJECT

public:
    explicit HighlightEditor(QWidget *parent = 0, Highlight *highController=0);
    ~HighlightEditor();
    void accept();

protected slots:
    void selectFontColor();
    void selectBackgroundColor();
    void selectHighlight(QModelIndex);
    void addHighlight();
    void deletePresset();
    void addGroup();
    void deleteGroup();
    void selectGroup(QString);

    void boldChanged(bool);
    void caseChanged(bool);
    void trayChanged(bool);
    void mailChanged(bool);
    void wholeLineChanged(bool);

private:
    void updateExample();
    void setUpdateButtons();
    void updateCurrent();
    void resetInput();
    void addRow(HighlightParameters highlight);
    void insertHighlight();
    void updateHighlight();
    void addButtonToSave();
    void confirmSave();
    void readHiglights();
    void readGroups();
    HighlightParameters getCurrentConfigFromPointer();

    QPalette p;
    QFont f;
    Highlight *highLightController;
    Ui::HighlightEditor *ui;    
    QStandardItemModel *model;
    QStandardItem *item;
    HighlightParameters *currentHighlight;
    QString oldText;
    bool NewItem, Updated;
};

#endif // HIGHLIGHTEDITOR_H
