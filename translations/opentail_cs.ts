<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<context>
    <name>FileInfoFrame</name>
    <message>
        <location filename="../fileinfoframe.ui" line="26"/>
        <source>Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileinfoframe.ui" line="43"/>
        <source>Filename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileinfoframe.ui" line="50"/>
        <source>Last modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileinfoframe.ui" line="57"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileinfoframe.ui" line="68"/>
        <location filename="../fileinfoframe.ui" line="75"/>
        <location filename="../fileinfoframe.ui" line="82"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HighlightEditor</name>
    <message>
        <location filename="../highlighteditor.ui" line="14"/>
        <source>Highlight Editor</source>
        <translation>Editor zvýraznění</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="29"/>
        <source>&amp;Ignore case</source>
        <translation>&amp;Nerozlišovat velikost písmen</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="475"/>
        <source>Example</source>
        <translation>Příklad</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="502"/>
        <source>&amp;Font color</source>
        <translation>Barva &amp;písma</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="509"/>
        <source>&amp;Background color</source>
        <translation>Barva &amp;pozadí</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="516"/>
        <source>Bo&amp;ld</source>
        <translation>&amp;Tučné</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="556"/>
        <location filename="../highlighteditor.cpp" line="220"/>
        <location filename="../highlighteditor.cpp" line="230"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="563"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="611"/>
        <source>&amp;Add</source>
        <translation>&amp;Přidat</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="621"/>
        <source>&amp;Delete</source>
        <translation>&amp;Smazat</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="645"/>
        <source>Actions</source>
        <translation>Činnosti</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="651"/>
        <source>Show on &amp;tray</source>
        <translation>Ukázat v oznamovací oblasti &amp;panelu</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="658"/>
        <source>Send &amp;mail</source>
        <translation>Poslat e-&amp;mail</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="678"/>
        <source>Highlight whole line</source>
        <translation>Zvýraznit celý řádek</translation>
    </message>
    <message>
        <location filename="../highlighteditor.cpp" line="61"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../highlighteditor.cpp" line="204"/>
        <source>Save first</source>
        <translation>Nejprve uložit</translation>
    </message>
    <message>
        <location filename="../highlighteditor.cpp" line="205"/>
        <source>You have changes that are not yet saved.
Save first?</source>
        <translation>Máte neuložené změny.
Nejprve uložit?</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Hlavní okno</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="51"/>
        <source>&amp;File</source>
        <translation>&amp;Soubor</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="62"/>
        <source>&amp;Settings</source>
        <translation>&amp;Nastavení</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="71"/>
        <source>&amp;Window</source>
        <translation>&amp;Okno</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="82"/>
        <source>&amp;Help</source>
        <translation>Nápo&amp;věda</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="116"/>
        <source>Search for</source>
        <translation>Hledat</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="126"/>
        <source>Search</source>
        <translation>Hledat</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="135"/>
        <source>&amp;Project</source>
        <translation>&amp;Projekt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="140"/>
        <source>&amp;Add File</source>
        <translation>Při&amp;dat soubor</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="143"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="148"/>
        <source>&amp;Quit</source>
        <translation>&amp;Ukončit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="151"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="156"/>
        <source>&amp;Highlight</source>
        <translation>&amp;Zvýraznit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="161"/>
        <source>&amp;Options</source>
        <translation>&amp;Volby</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="166"/>
        <source>&amp;Cascade</source>
        <translation>&amp;Překrývat</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="171"/>
        <source>&amp;Tile</source>
        <translation>Klást jedno &amp;vedle druhého</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="176"/>
        <source>&amp;Close</source>
        <translation>&amp;Zavřít</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="181"/>
        <source>Close &amp;All</source>
        <translation>Zavřít &amp;vše</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="189"/>
        <source>&amp;Close File</source>
        <translation>Z&amp;avřít soubor</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="194"/>
        <source>Select &amp;All</source>
        <translation>Vyb&amp;rat vše</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="199"/>
        <source>&amp;About</source>
        <translation>&amp;O programu</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="204"/>
        <source>About &amp;Qt</source>
        <translation>O &amp;Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="212"/>
        <source>&amp;Hide toolbar</source>
        <translation>&amp;Skrýt nástrojový pruh</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="215"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="223"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pozastavit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="226"/>
        <source>Ctrl+Shift+P</source>
        <translation>Ctrl+Shift+P</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="234"/>
        <source>Tabbed &amp;view</source>
        <translation>Po&amp;hled s kartami</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="239"/>
        <source>Show dock</source>
        <translation>Ukázat panel</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="166"/>
        <source>Select file</source>
        <translation>Vybrat soubor</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <source>About OpenTail</source>
        <translation>O programu OpenTail</translation>
    </message>
</context>
<context>
    <name>NewHighlight</name>
    <message>
        <location filename="../newhighlight.ui" line="14"/>
        <source>Text to search for</source>
        <translation>Text k vyhledání</translation>
    </message>
    <message>
        <location filename="../newhighlight.ui" line="22"/>
        <source>Search for:</source>
        <translation>Hledat:</translation>
    </message>
    <message>
        <location filename="../newhighlight.cpp" line="27"/>
        <source>Highlight group name</source>
        <translation>Název skupiny zvýraznění</translation>
    </message>
    <message>
        <location filename="../newhighlight.cpp" line="28"/>
        <source>Group name</source>
        <translation>Název skupiny</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="34"/>
        <source>Systray</source>
        <translation>Oznamovací oblast panelu</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="35"/>
        <source>I couldn&apos;t detect any system tray on this system.</source>
        <translation>V systému se nepodařilo zjistit žádnou oznamovací oblast panelu.</translation>
    </message>
</context>
<context>
    <name>QRecentFilesMenu</name>
    <message>
        <location filename="../QRecentFilesMenu.cpp" line="150"/>
        <source>Clear Menu</source>
        <translation>Smazat</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="31"/>
        <source>Time between scans</source>
        <translation>Čas mezi prohledáváními</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="38"/>
        <source>Mail settings</source>
        <translation>Nastavení pošty</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="44"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="54"/>
        <source>From email</source>
        <translation>Z e-mailu</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="64"/>
        <source>SMTP server</source>
        <translation>Server SMTP</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="74"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="84"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="99"/>
        <source>Max buffer size</source>
        <translation>Největší velikost vyrovnávací paměti</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="109"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="128"/>
        <source>Font</source>
        <translation>Písmo</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="134"/>
        <source>Select font</source>
        <translation>Vybrat písmo</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="141"/>
        <source>&amp;Font color</source>
        <translation>Barva &amp;písma</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="148"/>
        <source>&amp;Background color</source>
        <translation>Barva &amp;pozadí</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="588"/>
        <source>Example</source>
        <translation>Příklad</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="601"/>
        <source>Wrap lines</source>
        <translation>Zalomit řádky</translation>
    </message>
</context>
<context>
    <name>Smtp</name>
    <message>
        <location filename="../smtp.cpp" line="204"/>
        <source>Message sent</source>
        <translation>Zpráva poslána</translation>
    </message>
    <message>
        <location filename="../smtp.cpp" line="214"/>
        <source>Qt Simple SMTP client</source>
        <translation>Jednoduchý SMTP klient Qt</translation>
    </message>
    <message>
        <location filename="../smtp.cpp" line="214"/>
        <source>Unexpected reply from SMTP server:

</source>
        <translation>Neočekávaná odpověď od serveru SMTP:</translation>
    </message>
    <message>
        <location filename="../smtp.cpp" line="216"/>
        <source>Failed to send message</source>
        <translation>Nepodařilo se odeslat zprávu</translation>
    </message>
</context>
<context>
    <name>TailWidget</name>
    <message>
        <location filename="../tailwidget.cpp" line="30"/>
        <source>Select highlight group</source>
        <translation>Vybrat skupinu zvýraznění</translation>
    </message>
    <message>
        <location filename="../tailwidget.cpp" line="34"/>
        <source>Filter</source>
        <translation>Filtr</translation>
    </message>
    <message>
        <location filename="../tailwidget.cpp" line="42"/>
        <source>Pause</source>
        <translation>Pozastavit</translation>
    </message>
    <message>
        <location filename="../tailwidget.cpp" line="121"/>
        <source>File does not exist</source>
        <translation>Soubor neexistuje</translation>
    </message>
    <message>
        <location filename="../tailwidget.cpp" line="121"/>
        <location filename="../tailwidget.cpp" line="129"/>
        <source>Unable to load file: </source>
        <translation>Nelze nahrát soubor: </translation>
    </message>
    <message>
        <location filename="../tailwidget.cpp" line="129"/>
        <source>Unable to open file</source>
        <translation>Nelze otevřít soubor</translation>
    </message>
</context>
<context>
    <name>TextWidget</name>
    <message>
        <location filename="../textwidget.cpp" line="14"/>
        <source>Clear</source>
        <translation>Vyprázdnit</translation>
    </message>
</context>
<context>
    <name>TrayLauncher</name>
    <message>
        <location filename="../traylauncher.cpp" line="28"/>
        <source>Click to open window</source>
        <translation>Klepnout pro otevření okna</translation>
    </message>
    <message>
        <location filename="../traylauncher.cpp" line="34"/>
        <source>Mi&amp;nimize</source>
        <translation>Zmen&amp;šit</translation>
    </message>
    <message>
        <location filename="../traylauncher.cpp" line="37"/>
        <source>Ma&amp;ximize</source>
        <translation>Zvě&amp;tšit</translation>
    </message>
    <message>
        <location filename="../traylauncher.cpp" line="40"/>
        <source>&amp;Restore</source>
        <translation>&amp;Obnovit</translation>
    </message>
    <message>
        <location filename="../traylauncher.cpp" line="43"/>
        <source>&amp;Quit</source>
        <translation>&amp;Ukončit</translation>
    </message>
</context>
</TS>
