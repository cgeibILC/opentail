<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="da_DK">
<context>
    <name>FileInfoFrame</name>
    <message>
        <location filename="../fileinfoframe.ui" line="26"/>
        <source>Frame</source>
        <translation>Ramme</translation>
    </message>
    <message>
        <location filename="../fileinfoframe.ui" line="43"/>
        <source>Filename</source>
        <translation>Filnavn</translation>
    </message>
    <message>
        <location filename="../fileinfoframe.ui" line="50"/>
        <source>Last modified</source>
        <translation>Sidst modificeret</translation>
    </message>
    <message>
        <location filename="../fileinfoframe.ui" line="57"/>
        <source>Size</source>
        <translation>Størrelse</translation>
    </message>
    <message>
        <location filename="../fileinfoframe.ui" line="68"/>
        <location filename="../fileinfoframe.ui" line="75"/>
        <location filename="../fileinfoframe.ui" line="82"/>
        <source>TextLabel</source>
        <translation>Tekst skilt</translation>
    </message>
</context>
<context>
    <name>HighlightEditor</name>
    <message>
        <location filename="../highlighteditor.ui" line="14"/>
        <source>Highlight Editor</source>
        <translation>Fræmhevelses regigering</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="29"/>
        <source>&amp;Ignore case</source>
        <translation>&amp;Ingorer størrelse</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="475"/>
        <source>Example</source>
        <translation>Eksempel</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="502"/>
        <source>&amp;Font color</source>
        <translation>Tekst &amp;farve</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="509"/>
        <source>&amp;Background color</source>
        <translation>&amp;Baggrunds farve</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="516"/>
        <source>Bo&amp;ld</source>
        <translation>&amp;Fed</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="556"/>
        <location filename="../highlighteditor.cpp" line="220"/>
        <location filename="../highlighteditor.cpp" line="230"/>
        <source>Add</source>
        <translation>Tilføj</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="563"/>
        <source>Delete</source>
        <translation>Slet</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="611"/>
        <source>&amp;Add</source>
        <translation>&amp;Tilføj</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="621"/>
        <source>&amp;Delete</source>
        <translation>&amp;Slet</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="645"/>
        <source>Actions</source>
        <translation>Handlinger</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="651"/>
        <source>Show on &amp;tray</source>
        <translation>&amp;Vis i skuffe</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="658"/>
        <source>Send &amp;mail</source>
        <translation>Send &amp;email</translation>
    </message>
    <message>
        <location filename="../highlighteditor.ui" line="678"/>
        <source>Highlight whole line</source>
        <translation>Frenhæv linie</translation>
    </message>
    <message>
        <location filename="../highlighteditor.cpp" line="61"/>
        <source>Save</source>
        <translation>Gem</translation>
    </message>
    <message>
        <location filename="../highlighteditor.cpp" line="204"/>
        <source>Save first</source>
        <translation>Gem først</translation>
    </message>
    <message>
        <location filename="../highlighteditor.cpp" line="205"/>
        <source>You have changes that are not yet saved.
Save first?</source>
        <translation>Der er ændringer som endnu ikke er gemt. Skal vi gemme først?</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Hovedvindue
</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="51"/>
        <source>&amp;File</source>
        <translation>&amp;Fil</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="62"/>
        <source>&amp;Settings</source>
        <translation>&amp;Indstillinger</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="71"/>
        <source>&amp;Window</source>
        <translation>&amp;Vinduer</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="82"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjælp</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="116"/>
        <source>Search for</source>
        <translation>Søg efter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="126"/>
        <source>Search</source>
        <translation>Søg</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="135"/>
        <source>&amp;Project</source>
        <translation>&amp;Projekt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="140"/>
        <source>&amp;Add File</source>
        <translation>&amp;Tilføj fil</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="143"/>
        <source>Ctrl+A</source>
        <translation>Control+A</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="148"/>
        <source>&amp;Quit</source>
        <translation>&amp;Afslut</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="151"/>
        <source>Ctrl+Q</source>
        <translation>Control+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="156"/>
        <source>&amp;Highlight</source>
        <translation>&amp;Fremhæv</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="161"/>
        <source>&amp;Options</source>
        <translation>&amp;Muligheder</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="166"/>
        <source>&amp;Cascade</source>
        <translation>&amp;Stable</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="171"/>
        <source>&amp;Tile</source>
        <translation>&amp;Tile</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="176"/>
        <source>&amp;Close</source>
        <translation>&amp;Luk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="181"/>
        <source>Close &amp;All</source>
        <translation>Luk &amp;Alle</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="189"/>
        <source>&amp;Close File</source>
        <translation>Luk &amp;Fil</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="194"/>
        <source>Select &amp;All</source>
        <translation>&amp;Vælg Alle</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="199"/>
        <source>&amp;About</source>
        <translation>&amp;Om</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="204"/>
        <source>About &amp;Qt</source>
        <translation>Om &amp;Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="212"/>
        <source>&amp;Hide toolbar</source>
        <translation>&amp;Skjul værktøjslinie</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="215"/>
        <source>Ctrl+M</source>
        <translation>Control+M</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="223"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="226"/>
        <source>Ctrl+Shift+P</source>
        <translation>Control+Skift+P</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="234"/>
        <source>Tabbed &amp;view</source>
        <translation>&amp;Faneblad</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="239"/>
        <source>Show dock</source>
        <translation>Vis dock</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="166"/>
        <source>Select file</source>
        <translation>Vælg fil</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <source>About OpenTail</source>
        <translation>Om Opentail</translation>
    </message>
</context>
<context>
    <name>NewHighlight</name>
    <message>
        <location filename="../newhighlight.ui" line="14"/>
        <source>Text to search for</source>
        <translation>Tekst at søge efter</translation>
    </message>
    <message>
        <location filename="../newhighlight.ui" line="22"/>
        <source>Search for:</source>
        <translation>Søg efter:</translation>
    </message>
    <message>
        <location filename="../newhighlight.cpp" line="27"/>
        <source>Highlight group name</source>
        <translation>Gruppenavn for femhævning</translation>
    </message>
    <message>
        <location filename="../newhighlight.cpp" line="28"/>
        <source>Group name</source>
        <translation>Gruppenavn</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="26"/>
        <source>Systray</source>
        <translation>Systremskuffe</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="27"/>
        <source>I couldn&apos;t detect any system tray on this system.</source>
        <translation>Jeg kunne ikke finde nogen systemskuffe på systemet.</translation>
    </message>
</context>
<context>
    <name>QRecentFilesMenu</name>
    <message>
        <location filename="../QRecentFilesMenu.cpp" line="150"/>
        <source>Clear Menu</source>
        <translation>Slet liste</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Indstillinger</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="31"/>
        <source>Time between scans</source>
        <translation>Tid mellem indlæsninger</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="38"/>
        <source>Mail settings</source>
        <translation>e-mail indstillinger</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="44"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="54"/>
        <source>From email</source>
        <translation>Fra</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="64"/>
        <source>SMTP server</source>
        <translation>SMTP server</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="74"/>
        <source>Username</source>
        <translation>Brugernavn</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="84"/>
        <source>Password</source>
        <translation>Adgangskode</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="99"/>
        <source>Max buffer size</source>
        <translation>Max. buffer størrelse</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="109"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="128"/>
        <source>Font</source>
        <translation>Skrift</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="134"/>
        <source>Select font</source>
        <translation>Vælg skrift</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="141"/>
        <source>&amp;Font color</source>
        <translation>Tekst &amp;farve</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="148"/>
        <source>&amp;Background color</source>
        <translation>&amp;Baggrunds farve</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="588"/>
        <source>Example</source>
        <translation>Eksempel</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="601"/>
        <source>Wrap lines</source>
        <translation>Opbryd linier</translation>
    </message>
</context>
<context>
    <name>Smtp</name>
    <message>
        <location filename="../smtp.cpp" line="204"/>
        <source>Message sent</source>
        <translation>Besked sendt</translation>
    </message>
    <message>
        <location filename="../smtp.cpp" line="214"/>
        <source>Qt Simple SMTP client</source>
        <translation>QT simpel klient</translation>
    </message>
    <message>
        <location filename="../smtp.cpp" line="214"/>
        <source>Unexpected reply from SMTP server:

</source>
        <translation>Uforventet svar fra server
</translation>
    </message>
    <message>
        <location filename="../smtp.cpp" line="216"/>
        <source>Failed to send message</source>
        <translation>Kunne ikke sende besked</translation>
    </message>
</context>
<context>
    <name>TailWidget</name>
    <message>
        <location filename="../tailwidget.cpp" line="30"/>
        <source>Select highlight group</source>
        <translation>Vælg fremhævelses gruppe</translation>
    </message>
    <message>
        <location filename="../tailwidget.cpp" line="34"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../tailwidget.cpp" line="42"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../tailwidget.cpp" line="121"/>
        <source>File does not exist</source>
        <translation>Filen findes ikke</translation>
    </message>
    <message>
        <location filename="../tailwidget.cpp" line="121"/>
        <location filename="../tailwidget.cpp" line="129"/>
        <source>Unable to load file: </source>
        <translation>Kan ikke læse filen:</translation>
    </message>
    <message>
        <location filename="../tailwidget.cpp" line="129"/>
        <source>Unable to open file</source>
        <translation>Kan ikke åbne filen</translation>
    </message>
</context>
<context>
    <name>TextWidget</name>
    <message>
        <location filename="../textwidget.cpp" line="14"/>
        <source>Clear</source>
        <translation>Slet</translation>
    </message>
</context>
<context>
    <name>TrayLauncher</name>
    <message>
        <location filename="../traylauncher.cpp" line="28"/>
        <source>Click to open window</source>
        <translation>Klik for at åbne vindue</translation>
    </message>
    <message>
        <location filename="../traylauncher.cpp" line="34"/>
        <source>Mi&amp;nimize</source>
        <translation>M&amp;Minimér</translation>
    </message>
    <message>
        <location filename="../traylauncher.cpp" line="37"/>
        <source>Ma&amp;ximize</source>
        <translation>Ma&amp;ximer</translation>
    </message>
    <message>
        <location filename="../traylauncher.cpp" line="40"/>
        <source>&amp;Restore</source>
        <translation>&amp;Gendan</translation>
    </message>
    <message>
        <location filename="../traylauncher.cpp" line="43"/>
        <source>&amp;Quit</source>
        <translation>&amp;Afslut</translation>
    </message>
</context>
</TS>
