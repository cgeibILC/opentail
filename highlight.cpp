#include "highlight.h"
#include <QtGui>

Highlight::Highlight()
{
    settings = 0;
}

void Highlight::load()
{
    int size = settings->beginReadArray("highlights");
    for (int i = 0; i < size; ++i) {
        settings->setArrayIndex(i);
        HighlightParameters h;
        h.fontColor = settings->value("fontColor").toString();
        h.backgroundColor = settings->value("backgroundColor").toString();
        h.searchFor = settings->value("searchFor").toString();
        h.bold = settings->value("bold", false).toBool();
        h.ignoreCase = settings->value("ignorecase", false).toBool();
        h.tray = settings->value("tray", false).toBool();
        h.mail = settings->value("mail", false).toBool();
        h.wholeLine= settings->value("wholeline", false).toBool();
        append(h);
    }
    settings->endArray();
}

void Highlight::append(HighlightParameters h)
{
    highlights.append(h);
}

void Highlight::update(HighlightParameters h, QString searchString)
{
    int position;
    if (searchString == 0){
        position = find(h.searchFor);
    }else{
        position = find(searchString);
    }
    if (position >= 0){
        highlights.replace(position, h);
    }
}

int Highlight::find(QString name)
{
    for (int i = 0; i<highlights.count(); i++){
        if (highlights.at(i).searchFor.compare(name)==0){

            return i;
        }
    }

    return -1;
}

void Highlight::remove(QString name)
{
    highlights.removeAt(find(name));
}

void Highlight::save()
{
    if (highlights.count()==0){
        return;
    }
    settings->beginWriteArray("highlights");
    //Save all highlights
    Highlight h;
    for (int i = 0; i<highlights.count(); i++){
        settings->setArrayIndex(i);
        settings->setValue("fontColor", highlights.at(i).fontColor.name());
        settings->setValue("backgroundColor", highlights.at(i).backgroundColor.name());
        settings->setValue("searchFor", highlights.at(i).searchFor);
        settings->setValue("bold", highlights.at(i).bold);
        settings->setValue("ignorecase", highlights.at(i).ignoreCase);
        settings->setValue("tray", highlights.at(i).tray);
        settings->setValue("mail", highlights.at(i).mail);
        settings->setValue("wholeline", highlights.at(i).wholeLine);
    }
    settings->endArray();
}

QString Highlight::getGroup()
{
    return settings->value("name", "default").toString();
}

void Highlight::setGroup(QString groupName)
{
    qDebug("Load group: "+groupName.toLatin1());
    if (groupName.length()){
        settings = new QSettings(QCoreApplication::applicationName(), groupName+"_hlg");
        if (settings->value("name", "").toString().length()==0){
            settings->setValue("name", groupName);
        }
        settings->sync();
        highlights.clear();
        load();
    }
}

void Highlight::removeGroup(QString groupName)
{
    QSettings s(QCoreApplication::applicationName(), groupName+"_hlg");
    QFile f(s.fileName());
    f.remove();
}

QStringList Highlight::listGroups()
{
    if (!settings) {
        setGroup("default");
    }
    QFileInfo fi(settings->fileName());
    QDir dir(fi.path());
    dir.setFilter(QDir::Files);
    QStringList filters;
    filters << "*_hlg*";
    dir.setNameFilters(filters);
    QStringList sl;
    QFileInfoList list = dir.entryInfoList();
    if (list.size()!=0){
        for (int i = 0; i < list.size(); ++i) {
            QFileInfo fileInfo = list.at(i);
            sl.append(fileInfo.baseName().mid(0,fileInfo.baseName().indexOf("_") ));
        }
    } else {
        sl.append("default");
    }
    return sl;
}
