#include "highlighteditor.h"
#include "ui_highlighteditor.h"

HighlightEditor::HighlightEditor(QWidget *parent, Highlight * highController) :
    QDialog(parent),
    ui(new Ui::HighlightEditor)
{
    Updated = false;
    NewItem = false;
    currentHighlight = new HighlightParameters;
    ui->setupUi(this);
    highLightController = highController;
    readGroups();
    model = new QStandardItemModel();

    connect(ui->fontColor, SIGNAL(clicked()), this, SLOT(selectFontColor()));
    connect(ui->backgroundColor, SIGNAL(clicked()), this, SLOT(selectBackgroundColor()));
    connect(ui->highlightList, SIGNAL(clicked(QModelIndex)), this, SLOT(selectHighlight(QModelIndex)));
    connect(ui->addButton, SIGNAL(clicked()), this, SLOT(addHighlight()));
    connect(ui->bold, SIGNAL(clicked(bool)), this, SLOT(boldChanged(bool)));
    connect(ui->caseSensitive, SIGNAL(clicked(bool)), this, SLOT(caseChanged(bool)));
    connect(ui->trayNotification, SIGNAL(clicked(bool)), this, SLOT(trayChanged(bool)));
    connect(ui->sendMail, SIGNAL(clicked(bool)), this, SLOT(mailChanged(bool)));
    connect(ui->wholeLine, SIGNAL(clicked(bool)), this, SLOT(wholeLineChanged(bool)));
    connect(ui->deleteButton, SIGNAL(clicked()), this, SLOT(deletePresset()));
    connect(ui->addHighligtGroup, SIGNAL(clicked()), this, SLOT(addGroup()));
    connect(ui->deleteHighlightGroup, SIGNAL(clicked()), this, SLOT(deleteGroup()));
    connect(ui->highlightGroup, SIGNAL(activated(QString)), this, SLOT(selectGroup(QString)));

    ui->highlightList->setModel( model );
    selectGroup(ui->highlightGroup->currentText());
    //readHiglights();
}

HighlightParameters HighlightEditor::getCurrentConfigFromPointer()
{
    HighlightParameters h;
    h.backgroundColor = currentHighlight->backgroundColor;
    h.bold = currentHighlight->bold;
    h.fontColor = currentHighlight->fontColor;
    h.ignoreCase = currentHighlight->ignoreCase;
    h.mail = currentHighlight->mail;
    h.searchFor = currentHighlight->searchFor;
    h.tray = currentHighlight->tray;
    h.wholeLine = currentHighlight->wholeLine;
    return h;
}

void HighlightEditor::readGroups()
{
    ui->highlightGroup->clear();
    ui->highlightGroup->addItems(highLightController->listGroups());
    int index = ui->highlightGroup->findText("default");
    if ( index != -1 ) {
       ui->highlightGroup->setCurrentIndex(index);
    }
}

void HighlightEditor::resetInput()
{
    ui->caseSensitive->setChecked(false);
    ui->bold->setChecked(false);
    ui->trayNotification->setChecked(false);
    ui->sendMail->setChecked(false);
    ui->wholeLine->setChecked(false);
}

void HighlightEditor::addButtonToSave()
{
    ui->addButton->setText(tr("Save"));
    ui->addButton->setDisabled(false);
    Updated = true;
}

void HighlightEditor::addHighlight()
{
    if (NewItem) {
        insertHighlight();
    }else if(Updated) {
        updateHighlight();
    }else{
        NewHighlight highlightName;
        if (highlightName.exec()){
            resetInput();
            NewItem = true;
            currentHighlight = new HighlightParameters;
            currentHighlight->bold = false;
            currentHighlight->backgroundColor = "#000";
            currentHighlight->fontColor = "#fff";
            currentHighlight->ignoreCase = false;
            currentHighlight->mail = false;
            currentHighlight->tray = false;
            currentHighlight->wholeLine = false;
            currentHighlight->searchFor = highlightName.getHighlightText().toLatin1();
            addButtonToSave();
            updateExample();
        }
    }
}


void HighlightEditor::boldChanged(bool check)
{
    if (!currentHighlight) {
        return;
    }
    currentHighlight->bold = check;
    updateExample();
    addButtonToSave();
}

void HighlightEditor::caseChanged(bool check)
{
    if (!currentHighlight) {
        return;
    }
    currentHighlight->ignoreCase = check;
    updateExample();
    addButtonToSave();
}

void HighlightEditor::trayChanged(bool check)
{
    if (!currentHighlight) {
        return;
    }
    currentHighlight->tray = check;
    updateExample();
    addButtonToSave();
}

void HighlightEditor::mailChanged(bool check)
{
    if (!currentHighlight) {
        return;
    }
    currentHighlight->mail = check;
    updateExample();
    addButtonToSave();
}

void HighlightEditor::wholeLineChanged(bool check)
{
    if (!currentHighlight) {
        return;
    }
    currentHighlight->wholeLine = check;
    updateExample();
    addButtonToSave();
}

void HighlightEditor::selectFontColor()
{
    if (!currentHighlight) {
        return;
    }
    QColor color = QColorDialog::getColor(currentHighlight->fontColor, this);
    if (color.isValid())
    {
        currentHighlight->fontColor = color;
    }

    updateExample();
    addButtonToSave();
}

void HighlightEditor::selectBackgroundColor()
{
    if (!currentHighlight) {
        return;
    }
    QColor color = QColorDialog::getColor(currentHighlight->backgroundColor, this);
    if (color.isValid())
    {
        currentHighlight->backgroundColor = color;
    }
    updateExample();
    addButtonToSave();
}

void HighlightEditor::deletePresset()
{
    highLightController->remove(currentHighlight->searchFor);
    model->removeRow(ui->highlightList->currentIndex().row());
    ui->deleteButton->setDisabled(true);
    resetInput();
}

void HighlightEditor::selectHighlight(QModelIndex index)
{
    confirmSave();
    HighlightParameters h = highLightController->getHighlights().at(index.row());
    currentHighlight->searchFor = h.searchFor;
    currentHighlight->backgroundColor = h.backgroundColor;
    currentHighlight->bold = h.bold;
    currentHighlight->fontColor = h.fontColor;
    currentHighlight->ignoreCase = h.ignoreCase;
    currentHighlight->mail = h.mail;
    currentHighlight->tray = h.tray;
    currentHighlight->wholeLine = h.wholeLine;
    ui->bold->setChecked(currentHighlight->bold);
    ui->trayNotification->setChecked(currentHighlight->tray);
    ui->caseSensitive->setChecked(currentHighlight->ignoreCase);
    ui->sendMail->setChecked(currentHighlight->mail);
    ui->wholeLine->setChecked(currentHighlight->wholeLine);
    ui->deleteButton->setDisabled(false);
    updateExample();
}

void HighlightEditor::confirmSave()
{
    if ((Updated || NewItem) &&
            QMessageBox::warning(this, tr("Save first"),
                                 tr("You have changes that are not yet saved.\nSave first?"),
                                 QMessageBox::Save, QMessageBox::No) == QMessageBox::QMessageBox::Save){
        if (NewItem) {
            insertHighlight();
        }else if(Updated){
            updateHighlight();
        }
    }
}

void HighlightEditor::insertHighlight()
{
    if (!currentHighlight) {
        return;
    }
    ui->addButton->setText(tr("Add"));
    HighlightParameters h = getCurrentConfigFromPointer();
    addRow(h);
    highLightController->append(h);
    NewItem = false;
    Updated = false;
}

void HighlightEditor::updateHighlight()
{
    ui->addButton->setText(tr("Add"));
    highLightController->update(getCurrentConfigFromPointer());
    Updated = false;
    NewItem = false;
}

void HighlightEditor::addRow(HighlightParameters highlight)
{
    item = new QStandardItem();
    item->setData( highlight.searchFor, Qt::DisplayRole );
    item->setData( QImage(":/Pix/Pix.png"), Qt::DecorationRole );
    item->setEditable( false );
    model->appendRow( item );
}

void HighlightEditor::updateExample()
{
    if (!currentHighlight) {
        return;
    }

    p = ui->exampleFrame->palette();
    p.setColor(QPalette::Background, currentHighlight->backgroundColor);
    p.setColor(QPalette::Foreground, currentHighlight->fontColor);
    ui->exampleFrame->setPalette(p);
    f.setBold(currentHighlight->bold);
    ui->example->setFont(f);
    ui->example->setText(currentHighlight->searchFor);
}

void HighlightEditor::addGroup()
{
    NewHighlight highlightName;
    highlightName.setType(1);
    if (highlightName.exec()){
        highLightController->setGroup(highlightName.getHighlightText());
        readGroups();
    }
}

void HighlightEditor::deleteGroup()
{
    if (!ui->highlightGroup->currentText().isNull()){
        highLightController->removeGroup(ui->highlightGroup->currentText());
    }
    readGroups();
}

void HighlightEditor::selectGroup(QString groupName)
{
    confirmSave();
    highLightController->setGroup(groupName);
    readHiglights();
}

void HighlightEditor::readHiglights()
{
    model->clear();
    QList<HighlightParameters> highlights = highLightController->getHighlights();
    for (int i=0;i< highlights.count(); i++){
        addRow(highlights.at(i));
    }

    NewItem = false;
    Updated = false;
}

void HighlightEditor::accept()
{
    confirmSave();
    highLightController->save();
    QDialog::accept();
}


HighlightEditor::~HighlightEditor()
{

}
