#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QIcon>
#include <QTimer>
#include <QMdiSubWindow>
#include <QList>
#include <QListWidgetItem>
#include <QSettings>
#include <QMimeData>
#include <QMainWindow>

#include "fileinfoframe.h"
#include "tailwidget.h"
#include "highlighteditor.h"
#include "highlight.h"
#include "traylauncher.h"
#include "settingsdialog.h"
#include "filelist.h"
#include "smtp.h"


#include "QRecentFilesMenu.h"

namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

protected:
   // void dropEvent( QDropEvent * event );

protected slots:
    void addFile();
    void loadFile(QString fileName="", QString highlightGroup="default");
    void closeFile();
    void closeFile(QString fileToClose);
    void showHideToolbar();
    void optionsEditor();
    void itemDoubleClicked(QListWidgetItem *selectedFile);
    void highlightColorEditor();
    void enableCloseFile(QListWidgetItem *selectedFile);
    void reRead();
    void about();
    void aboutQt();
    void searchFor();    
    void subWindowActivated(QMdiSubWindow*);
    void viewAsTabs(bool);
    void closeApp();

private:
    void loadSettings();
    void saveSettings();
    QMdiSubWindow* getWindowFromName(QString fileName);
    TailWidget* selectFileFromName(QString fileName);
    QAction * searchAction;
    QStringList files;
    QString settingsFile;
    QSettings settings;
    QTimer *timer;
    QMdiSubWindow * currentWindow;
    QRecentFilesMenu *recentFileMenu;

    Ui::MainWindow *ui;
    TailWidget * widget, * tailWidget;
    TextWidget *currentDocument;
    QFrame * fileInfo;
    Highlight * highLightController;
    TrayLauncher * launcher;
    FileList * fileList;
    Smtp * smtpClient;
    FileInfoFrame * fileInfoFrame;

};

#endif // MAINWINDOW_H
