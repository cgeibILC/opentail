#ifndef TEXTWIDGET_H
#define TEXTWIDGET_H

#include <QMessageBox>
#include <QPlainTextEdit>
#include <QMouseEvent>
#include <QTimer>
#include <QPen>
#include <QPainter>
#include <QFontMetrics>
#include <math.h>

class TextWidget : public QPlainTextEdit
{
    Q_OBJECT
public:
    explicit TextWidget(QWidget *parent = 0);
    void search(QString);
protected:
    void paintEvent(QPaintEvent *e);

signals:
    void windowClicked();

public slots:

protected:
    void mousePressEvent(QMouseEvent *);
    void contextMenuEvent(QContextMenuEvent *event);
    void resizeEvent ( QResizeEvent * event );
private:
    bool markAsClicked;
    QList <int> lines;
    QRect rect;
    QPainter p;
};

#endif // TEXTWIDGET_H
