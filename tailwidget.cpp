#include "tailwidget.h"

TailWidget::TailWidget(QWidget *parent, QString newGroupName) :
    QWidget(parent)
{
    bool wrap = false;
    paused = false;
    fileName = "";
    fileHasChanged = false;
    initial = true;

    setLayout(new QGridLayout());
    QHBoxLayout *selectLayout = new QHBoxLayout();
    selectLayout->setSpacing(4);

    highlightController = new Highlight();
    highlightController->setGroup(newGroupName);
    layout()->addItem(selectLayout);
    layout()->addWidget(textView = new TextWidget(this));


    highlightSelector = new QComboBox(this);
    highlightSelector->addItems(highlightController->listGroups());

    int index = highlightSelector->findText(highlightController->getGroup());
    if ( index != -1 ) {
        highlightSelector->setCurrentIndex(index);
    }

    QLabel *selectLabel = new QLabel(tr("Select highlight group"), this);
    selectLayout->addWidget(selectLabel);
    selectLayout->addWidget(highlightSelector);

    QLabel *filterLabel = new QLabel(tr("Filter"), this);
    QComboBox *filterString = new QComboBox(this);
    filterString->setEditable(true);

    selectLayout->addWidget(filterLabel);
    selectLayout->addWidget(filterString);

    pauseBtn = new QToolButton(this);
    pauseBtn->setText(tr("Pause"));
    pauseBtn->setIcon(QIcon(":/pause.png"));
    pauseBtn->setCheckable(true);
    selectLayout->addWidget(pauseBtn);

    p = textView->palette();
    p.setColor(QPalette::Base, settings.value("backgroundcolor", "#000").toString());
    p.setColor(QPalette::Text, settings.value("fontcolor", "#DDD").toString());
    textView->setPalette(p);

    f.fromString(settings.value("font").toString());
    textView->setFont(f);
    textView->setReadOnly(true);
    wrap = settings.value("wraplines", false).toBool();
    textView->setWordWrapMode(wrap ? QTextOption::WordWrap : QTextOption::NoWrap);
    highlighter = new Highlighter(textFormat, textView->document(), highlightController);

    connect(textView, SIGNAL(windowClicked()), this, SLOT(windowClicked()));
    connect(pauseBtn, SIGNAL(toggled(bool)), this, SLOT(pause(bool)));
    connect(highlighter, SIGNAL(foundString(QString)), this, SLOT(stringFound(QString)));
    connect(highlightSelector, SIGNAL(activated(QString)), this, SLOT(selectHighlight(QString)));    
    connect(filterString, SIGNAL(activated(QString)), this, SLOT(applyFilter(QString)));
    connect(filterString, SIGNAL(editTextChanged(QString)), this, SLOT(setFilter(QString)));
}

void TailWidget::applyFilter(QString s)
{
    filter = s;
    fileHasChanged = true;
    document()->clear();
    load(fileName);
}

void TailWidget::pause(bool newState)
{
    if (newState){
        pausePalette = pauseBtn->palette();
        pauseBtn->setIcon(QIcon(":/play.png"));
        p = pauseBtn->palette();
        p.setColor(QPalette::Button, QColor("red"));
        pauseBtn->setPalette(p);
        pauseBtn->setAutoFillBackground(true);
    }else{
        pauseBtn->setPalette(pausePalette);
        pauseBtn->setAutoFillBackground(true);
        pauseBtn->setIcon(QIcon(":/pause.png"));
    }
    paused = newState;
}

void TailWidget::windowClicked()
{
fileHasChanged = false;
setWindowTitleFromName();
}

void TailWidget::stringFound(QString searchString)
{
    emit hightlightFound(fileName, searchString);
}

void TailWidget::reHighlight()
{
    highlighter->selectGroup();
}

void TailWidget::selectHighlight(QString highlightGroup)
{
    highlighter->selectGroup(highlightGroup);
}

void TailWidget::setGroup(QString groupName )
{
    selectHighlight(groupName);
}

bool TailWidget::checkFile(QString fileName)
{
    if (!QFile::exists(fileName)){
        QMessageBox::critical(this, tr("File does not exist"), tr("Unable to load file: ")+fileName.toLatin1());

        return false;
    }else{
        file.setFileName(fileName);
    }

    if (!file.open(QFile::ReadOnly)){
        QMessageBox::critical(this, tr("Unable to open file"), tr("Unable to load file: ")+fileName.toLatin1());

        return false;
    }

    return true;
}

bool TailWidget::setFile(qint64 pos)
{
    QString str = "";

    lastPos = file.size();
    //Read the content of the file into the texteditor
    if ( file.seek(pos) ){
        while (!file.atEnd()){
            str = file.readLine();
            if (filter.length()>=4 && str.contains(filter)) {
                textView->insertPlainText(str);
            }else if(filter.length()<4){
                textView->insertPlainText(str);
            }else if(filter.length()>=4) {
            }
        }
        file.close();

    }else{
        qDebug("Could not seek to position");
    }
    QScrollBar *sb = textView->verticalScrollBar();
    sb->setValue(sb->maximum());
    return true;
}

void TailWidget::setFilter(QString f)
{
    if (f.length()==0){
        qDebug("here");
        filter = "";
        load(fileName);
    }
}

bool TailWidget::load(QString f)
{
    fileName = f;

    if (!checkFile(f)){

        return false;
    }
    int bufferSize = settings.value("buffersize", 10).toFloat()*(1024*1024);
    if (file.size() > bufferSize) {
        lastPos = file.size()-bufferSize;
    }else{
        lastPos = 0;
    }
    if (!setFile(lastPos)){

        return false;
    }

    setWindowTitleFromName();
    if (initial) {
        watcher = new QFileSystemWatcher();
        watcher->addPath(f);
        connect(watcher, SIGNAL(fileChanged(QString)), this, SLOT(fileChanged(QString)));
    }
    return true;
}

bool TailWidget::reRead()
{
    if (paused) {
        qDebug("View paused");
        return true;
    }
    if (fileHasChanged == false){

        return true;
    }
    if (!checkFile(fileName)){

        return false;
    }

    if(!setFile(lastPos)){

        return false;
    }

    if(fileName.length()>40){
        _tmpString = ".."+fileName.right(40);
    }
    setWindowTitleFromName(" *");
    fileHasChanged = false;

    return true;
}
void TailWidget::setWindowTitleFromName(QString s)
{
    if(fileName.length()>40){
        _tmpString = ".."+fileName.right(40);
    }else{
        _tmpString = fileName;
    }
    setWindowTitle(_tmpString.append(s));
}

QString TailWidget::getFileName()
{

    return fileName;
}

QString TailWidget::getGroup()
{
    return  highlightSelector->currentText();
}

void TailWidget::fileChanged(QString)
{    
    setChanged();
}

void TailWidget::closeEvent(QCloseEvent * )
{
    emit windowClosed(fileName);
}

TailWidget::~TailWidget()
{
    emit windowClosed(fileName);
    delete textView;
}
