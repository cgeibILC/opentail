#include "settingsdialog.h"
#include "ui_settingsdialog.h"

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    ui->wrapLines->setChecked(settings.value("wraplines", false).toBool());
    ui->scanTime->setValue(settings.value("scantime", 1).toInt());
    ui->smtpUser->setText(settings.value("smtpuser", "").toString());
    ui->smtpPassword->setText(settings.value("smtppassword", "").toString());
    ui->smtpServer->setText(settings.value("smtpserver", "localhost").toString());
    ui->personName->setText(settings.value("personname", "").toString());
    ui->mailFrom->setText(settings.value("mailfrom", "").toString());
    ui->shellExec->setText(settings.value("shell", "").toString());

    ui->bufferSize->setValue(settings.value("buffersize", "").toFloat());

    connect(ui->backgroundColorBtn, SIGNAL(clicked()), this, SLOT(backgroundColorClicked()));
    connect(ui->fontColorBtn, SIGNAL(clicked()), this, SLOT(fontColorClicked()));
    connect(ui->fontSelector, SIGNAL(clicked()), this, SLOT(fontSelector()));

    connect(ui->terminalFont, SIGNAL(clicked()), this, SLOT(terminalFontSelector()));
    connect(ui->chooseShell, SIGNAL(clicked()), this, SLOT(chooseShellExec()));
    updateExample();
}

void SettingsDialog::chooseShellExec()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select shell");
    if (fileName.isNull()){

    }else{
        settings.value("shell", fileName.toLatin1());
        ui->shellExec->setText(fileName);
    }
}

void SettingsDialog::updateExample()
{
    QFont f;
    f.fromString(settings.value("font").toString());
    ui->exampleText->setFont(f);

    p = ui->exampleFrame->palette();
    p.setColor(QPalette::Background, settings.value("backgroundcolor", "#FFF").toString());
    p.setColor(QPalette::Foreground, settings.value("fontcolor", "#000").toString());
    ui->exampleFrame->setPalette(p);
    ui->exampleText->setFont(f);
    ui->exampleText->setText("Example");
}

void SettingsDialog::fontSelector()
{
    bool ok;
    QFont font;
    font.fromString(settings.value("font").toString());
    font = QFontDialog::getFont(&ok, font, this);
    if (ok) {

        settings.setValue("font", font.toString());
        updateExample();
    }
}

void SettingsDialog::fontColorClicked()
{
    QColor color = QColorDialog::getColor(settings.value("fontcolor", "#FFF").toString(), this);
    if (color.isValid())
    {
        settings.setValue("fontcolor", color.name());
        updateExample();
    }

}

void SettingsDialog::backgroundColorClicked()
{
    QColor color = QColorDialog::getColor(settings.value("backgroundcolor", "#000").toString(), this);
    if (color.isValid())
    {
        settings.setValue("backgroundcolor", color.name());
        updateExample();
    }
}

void SettingsDialog::terminalFontSelector()
{
    bool ok;
    QFont font;
    font.fromString(settings.value("terminal-font").toString());
    font = QFontDialog::getFont(&ok, font, this);
    if (ok) {

        settings.setValue("terminal-font", font.toString());
    }
}


void SettingsDialog::accept()
{
    settings.setValue("wraplines", ui->wrapLines->isChecked());
    settings.setValue("scantime", ui->scanTime->value());

    settings.setValue("smtpuser", ui->smtpUser->text());
    settings.setValue("mailfrom", ui->mailFrom->text());
    settings.setValue("smtppassword", ui->smtpPassword->text());
    settings.setValue("smtpserver", ui->smtpServer->text());
    settings.setValue("personname", ui->personName->text());

    settings.setValue("buffersize", ui->bufferSize->value());
    QDialog::accept();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}
