#
# spec file for package qmmp
#
# Copyright (c) 2013 SUSE LINUX Products GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


%define soname 0

Name:           opentail
Version:        0.3
Release:        0
Summary:        Qt GUI based tail application
License:        GPL-2.0+
Group:          Utilities/Monitor
Url:            http://www.opentail.org/
Source0:        http://www.opentail.org/files/opentail-%{version}.tar.bz2
BuildRequires:  desktop-file-utils
BuildRequires:  gcc-c++
BuildRequires:  hicolor-icon-theme
BuildRequires:  pkgconfig(QtCore)
BuildRequires:  pkgconfig(QtDBus)
BuildRequires:  pkgconfig(QtGui)

%description
This program is an application with which you can tail log files with highlight, search and notification capabilities

%prep
%setup -q

%build
#mkdir build
#cd build
export CFLAGS='%{optflags}'
export CXXFLAGS='%{optflags}'
qmake &&  make %{?_smp_mflags}

%install
#cd build
export INSTALL_ROOT=$RPM_BUILD_ROOT
%makeinstall

%post
%icon_theme_cache_post
%desktop_database_post

%postun
%desktop_database_postun
%icon_theme_cache_postun

%files
%defattr(0644,root,root,0755)
%attr(0755,root,root) /%{_bindir}/opentail
%{_bindir}/opentail
%{_datadir}/applications/opentail.desktop
%{_datadir}/icons*

%changelog
* Thu Oct  3 2013 reddwarf@opensuse.org
- Update to 0.7.3
  * added window size and position saving to the projectm plugin
