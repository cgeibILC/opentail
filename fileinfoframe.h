#ifndef FILEINFOFRAME_H
#define FILEINFOFRAME_H

#include <QFileInfo>
#include <QDateTime>
#include <QFrame>

namespace Ui {
class FileInfoFrame;
}

class FileInfoFrame : public QFrame
{
    Q_OBJECT

public:
    explicit FileInfoFrame(QWidget *parent = 0);
    ~FileInfoFrame();
    void setFileName(QString);

private:
    Ui::FileInfoFrame *ui;


};

#endif // FILEINFOFRAME_H
