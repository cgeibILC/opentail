#ifndef TRAYLAUNCHER_H
#define TRAYLAUNCHER_H

#include <QSystemTrayIcon>
#include <QMessageBox>
#include <QMenu>
#include <QMainWindow>
#include <QApplication>

class TrayLauncher : public QDialog
{
    Q_OBJECT

public:
    TrayLauncher(QMainWindow * parent);

private slots:
    void alertFound(QString fileName, QString alertMessage);
    void iconClicked(QSystemTrayIcon::ActivationReason);

private:
    void createTrayIcon();
    void createActions();
    QMenu *trayMenu;
    QSystemTrayIcon * trayIcon;
    QMainWindow * parentWindow;

    QAction *minimizeAction;
    QAction *maximizeAction;
    QAction *restoreAction;
    QAction *quitAction;
    QAction *stopStart;
};

#endif // TRAYLAUNCHER_H
