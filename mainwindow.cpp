#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QFile>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    setWindowIcon(QIcon(":/tailerpng.svg"));

    ui->setupUi(this);
    //searchAction = ui->mainToolBar->addAction(QIcon(":/system-search.png"), tr("Search"), this, SLOT(search()));
    fileList = new FileList(this);
    ui->leftView->layout()->addWidget(fileList);

    fileInfoFrame = new FileInfoFrame();
    ui->leftView->layout()->addWidget(fileInfoFrame);

    launcher = new TrayLauncher(this);
    highLightController = new Highlight();


    recentFileMenu = new QRecentFilesMenu("Add Recent File");
    ui->menu_File->insertMenu(ui->menu_File->actions()[1], recentFileMenu);

    connect(fileList, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(itemDoubleClicked(QListWidgetItem*)));
    connect(fileList, SIGNAL(itemActivated(QListWidgetItem*)), this, SLOT(enableCloseFile(QListWidgetItem*)));
    connect(fileList, SIGNAL(openFile(QString)), this, SLOT(loadFile(QString)));

    connect(ui->action_About, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->actionAbout_Qt, SIGNAL(triggered()), this, SLOT(aboutQt()));
    connect(ui->action_Close_File, SIGNAL(triggered()), this, SLOT(closeFile()));
    connect(ui->action_Toolbars, SIGNAL(triggered()), this, SLOT(showHideToolbar()));
    connect(ui->actionTabbedView, SIGNAL(triggered(bool)), this, SLOT(viewAsTabs(bool)));

    connect(ui->searchBtn, SIGNAL(clicked()), this, SLOT(searchFor()));
    connect(ui->MDIView, SIGNAL(subWindowActivated(QMdiSubWindow*)), this, SLOT(subWindowActivated(QMdiSubWindow*)));


    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(reRead()));

    loadSettings();

    timer->start(settings.value("scantime", 5).toInt()*1000);
    ui->searchDock->hide();

}
void MainWindow::subWindowActivated(QMdiSubWindow* window)
{
    if (!window){
        return;
    }
    TailWidget * selectedWindow;
    selectedWindow = (TailWidget*)window->widget();
    fileInfoFrame->setFileName(selectedWindow->getFileName());
    fileList->selectFile(selectedWindow->getFileName());
}

/**
 * Enables and disables the close file menu item depending on
 * wether a file is selected or not
 *
 * @brief MainWindow::itemSelected
 * @param selectedFile QListWidgetItem
 */
void MainWindow::enableCloseFile(QListWidgetItem *selectedFile)
{
    if (selectedFile){
        ui->action_Close_File->setEnabled(true);
    }else{
        ui->action_Close_File->setEnabled(false);
    }
}

void MainWindow::highlightColorEditor()
{
    TailWidget *view;
    HighlightEditor * ColorEditor = new HighlightEditor(0, highLightController);
    if(ColorEditor->exec()){
        for (int j=0; j < ui->MDIView->subWindowList().size(); j++)
        {
            view = (TailWidget*)ui->MDIView->subWindowList()[j]->widget();
            view->reHighlight();
        }
    }
}

void MainWindow::optionsEditor()
{
    SettingsDialog optionsDialog;
    optionsDialog.exec();
}

/**
 * If an item is double clicked in the file list, call this and select the
 * corresponding window in the MDI
 *
 * @brief MainWindow::itemDoubleClicked
 * @param selectedFile QListWidgetItem
 */
void MainWindow::itemDoubleClicked(QListWidgetItem *selectedFile)
{
    if (selectedFile) {
        selectFileFromName(selectedFile->text());
    }
}

/**
 * Return the MDI window witch matches the filename given.
 *
 * @brief MainWindow::getWindowFromName
 * @param fileName QString
 * @return QMdiSubWindow The window selected OR NULL if no window match the fileName
 */
QMdiSubWindow* MainWindow::getWindowFromName(QString fileName)
{
    QMdiSubWindow * tmpWindow;
    for (int i = 0; i < ui->MDIView->subWindowList().count(); i++) {
        tmpWindow = ui->MDIView->subWindowList().at(i);
        if (((TailWidget*)tmpWindow->widget())->getFileName().compare(fileName) == 0 ){
            return tmpWindow;
        }
    }
    return NULL;
}

/**
 * @brief MainWindow::selectFileFromName
 * @param fileName Name of the file that we want to select
 * @return TailWidget The widget it self of the selected tailer
 */
TailWidget* MainWindow::selectFileFromName(QString fileName)
{
    QMdiSubWindow * tmpWindow;
    if ((tmpWindow = getWindowFromName(fileName))) {
        currentWindow = tmpWindow;
        currentDocument = ((TailWidget*)tmpWindow->widget())->document();
        ui->MDIView->setActiveSubWindow(tmpWindow);
        return (TailWidget*)tmpWindow->widget();

    }
    return NULL;
}

/**
 * Call this to reread all the files being tailed
 * This method is automatically called by the main timer
 * If the variable paused is set, it does nothing
 *
 * @brief MainWindow::reRead
 */
void MainWindow::reRead()
{
    for (int i=0; i < ui->MDIView->subWindowList().count();i++ ){
        ((TailWidget*)ui->MDIView->subWindowList().at(i)->widget())->reRead();
    }
}

/**
 * Exeutes an openfile dialog and calls loadFile with the name selected.
 * If the file already is loaded, it does nothing
 *
 * @brief MainWindow::addFile
 */
void MainWindow::addFile( )
{
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this, tr("Select file"), settings.value("lastdir", "~").toString());
    if (files.contains(fileName)){
        return;
    }
    QFileInfo fi(fileName);
    settings.setValue("lastdir", fi.absolutePath());

    loadFile(fileName);
}

/**
 * Creates a new tailer window and loads the content of the file
 * and connects all needed slots.
 *
 * @brief MainWindow::loadFile
 * @param fileName Name of the file to be tailed
 */
void MainWindow::loadFile(QString fileName, QString highlightGroup)
{    
    if (fileName.length()){
        QFileInfo file(fileName);

        widget = new TailWidget(0, highlightGroup);
        if ( (widget->load(fileName) == false) || ( file.exists() == false) ||
             file.isReadable() == false || file.isFile() == false ){
            files.removeOne(fileName);
            return;
        }
        //widget->selectHighlight(highlightGroup);

        recentFileMenu->addRecentFile(fileName);

        QListWidgetItem *fileItem = new QListWidgetItem(file.absoluteFilePath());
        fileList->addItem(fileItem);

        connect(widget, SIGNAL(hightlightFound(QString,QString)), launcher, SLOT(alertFound(QString,QString)));
        connect(widget, SIGNAL(windowClosed(QString)), this, SLOT(closeFile(QString)));

        ui->MDIView->addSubWindow(widget);
        widget->show();
        if (ui->actionTabbedView->isChecked() == false) {
            ui->MDIView->tileSubWindows();
        }
    }

    if (!files.contains(fileName)){
        files.append(fileName);
    }

    currentDocument = widget->document();
}

/**
 * Called by filelist to close a file.
 *
 * @brief MainWindow::closeFile
 */
void MainWindow::closeFile()
{
    if (fileList->currentItem()){
        getWindowFromName(fileList->currentItem()->text())->close();
    }

}

/**
 * Close a file based on a file name
 *
 * @brief MainWindow::closeFile
 * @param fileToClose QString File to close
 */
void MainWindow::closeFile(QString fileToClose)
{
    files.removeOne(fileToClose);
    for (int i =0; i < fileList->count(); i++){
        if (!fileList->item(i)->text().compare(fileToClose)){
            fileList->takeItem(i);
        }
    }
}

void MainWindow::loadSettings()
{
    restoreGeometry(settings.value("geometry").toByteArray());
    ui->mainToolBar->setVisible(settings.value("showmaintoolbar", true).toBool());
    ui->splitter->restoreState(settings.value("splitterSizes").toByteArray());
    restoreState(settings.value("mainwindow").toByteArray());
    viewAsTabs(settings.value("tabbed", false).toBool());
    ui->actionTabbedView->setChecked(settings.value("tabbed", false).toBool());

    int size = settings.beginReadArray("openfiles");
    for (int i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        files.append(settings.value("filename").toString());
        loadFile(settings.value("filename").toString(), settings.value("highlightgroup").toString());
    }
    settings.endArray();

    /*There is a part of me that is wondering if I should try and somehow integrate the
     *openfiles setting and the recent file menu together.  But since you could only have
     *1 open file from the last time and you want to open a file that you had previously
     *opened I think for the time being it makes sense to track them seperatly
     */
    if (settings.contains("recentfiles"))
    {
        recentFileMenu->restoreState(settings.value("recentfiles").toByteArray());
    }
    else
    {
        recentFileMenu->setMaxCount(10);
        recentFileMenu->setIcon(QIcon("://document-open-recent.svg"));
    }

    //#ifdef QT_VERSION >= 0x050000
    //new Qt 5 style typesafe connect
    //connect(recentFileMenu, &QRecentFilesMenu::recentFileTriggered, this, &MainWindow::loadFile);
    //#else
    //Qt 4 style connect
    connect(recentFileMenu, SIGNAL(recentFileTriggered(QString)), this, SLOT(loadFile(QString)));
    //#endif
}

void MainWindow::saveSettings()
{
    TailWidget *view;
    if (files.count()){
        settings.setValue("openFiles", files.join(","));
    }else{
        settings.setValue("openFiles", "");
    }
    QStringList openFiles;

    settings.beginWriteArray("openfiles");
    for (int j=0; j < ui->MDIView->subWindowList().size(); j++)
    {
        view = (TailWidget*)ui->MDIView->subWindowList()[j]->widget();
        settings.setArrayIndex(j);
        settings.setValue("filename", view->getFileName());
        settings.setValue("highlightgroup", view->getGroup());
    }
    settings.endArray();
    settings.setValue("geometry", saveGeometry());
    settings.setValue("showmaintoolbar", ui->mainToolBar->isVisible() ? "true" : "false");
    settings.setValue("splitterSizes", ui->splitter->saveState());
    settings.setValue("mainwindow", saveState());
    settings.setValue("tabbed", ui->actionTabbedView->isChecked() ? "true" : "false");

    settings.setValue("recentfiles", recentFileMenu->saveState());
}

void MainWindow::showHideToolbar()
{
    if (ui->mainToolBar->isVisible()){
        ui->mainToolBar->setVisible(false);
    }else{
        ui->mainToolBar->setVisible(true);
    }
}

void MainWindow::viewAsTabs(bool tabbedView)
{
    ui->action_Cascade->setDisabled(tabbedView);
    ui->action_Tile->setDisabled(tabbedView);
    if (tabbedView) {
        ui->MDIView->setViewMode(QMdiArea::TabbedView);
    }else{
        ui->MDIView->setViewMode(QMdiArea::SubWindowView);
    }
}

void MainWindow::searchFor()
{
    if (currentDocument){
        currentDocument->search(ui->searchString->text());
    }
}

void MainWindow::aboutQt()
{
    QMessageBox::aboutQt(this, "OpenTail build on Qt");
}

void MainWindow::about()
{
    QFile aboutFile(":/about.html");
    aboutFile.open(QIODevice::ReadOnly);
    QMessageBox::about(this, tr("About OpenTail"), aboutFile.readAll());
    aboutFile.close();
}

void MainWindow::closeApp()
{    
    saveSettings();
    QApplication::quit();
}

MainWindow::~MainWindow()
{
    delete ui;
}
