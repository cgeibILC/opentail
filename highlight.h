#ifndef HIGHLIGHT_H
#define HIGHLIGHT_H
#include <QColor>
#include <QSettings>

#include "HighlightParameters.h"

class Highlight
{

public:
    Highlight();
    QStringList listGroups();

    QString getGroup();
    void setGroup(QString groupName);
    void removeGroup(QString groupName);
    void setBacgroundColor(QColor c){ backgroundColor = c; }
    void setFontColor(QColor c){ fontColor = c; }
    void setSearchString(QString s){ searchString = s; }
    void append(HighlightParameters h);
    void update(HighlightParameters h, QString searchString = 0);
    int find(QString);
    void remove(QString);
    QList <HighlightParameters> getHighlights() { return highlights; }
    void save();

private:
    void load();
    QSettings *settings;
    QColor fontColor, backgroundColor;
    QString searchString;
    QList<HighlightParameters> highlights;
    int currentHighlight;
};

#endif // HIGHLIGHT_H
