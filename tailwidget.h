#ifndef TAILWIDGET_H
#define TAILWIDGET_H

#include <QMdiSubWindow>
#include <QFileSystemWatcher>
#include <QFile>
#include <QTextCodec>
#include <QMessageBox>
#include <QBoxLayout>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QTextStream>
#include <QPushButton>
#include <QToolButton>
#include <QAction>
#include <QScrollBar>

#include "highlighter.h"
#include "highlight.h"
#include "textwidget.h"

namespace Ui {
class TailWidget;
}

class TailWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TailWidget(QWidget *parent = 0, QString = 0);
    QString getFileName();
    QString getGroup();
    void setGroup(QString);
    bool load(QString f);
    void reHighlight();
    bool reRead();

    TextWidget *document() { return textView; };

    ~TailWidget();
protected:
    void closeEvent(QCloseEvent *);

signals:
    void windowClosed(QString);
    void hightlightFound(QString fileName, QString message);

private slots:
    void applyFilter(QString);
    void fileChanged(QString);
    void stringFound(QString);
    void setFilter(QString);
    void selectHighlight(QString);
    void pause(bool);
    void windowClicked();

private:
    void setChanged(){ fileHasChanged = true; }
    bool setFile(qint64 pos);
    void setWindowTitleFromName(QString s="");
    bool checkFile(QString fileName);

    bool fileHasChanged, paused, initial;
    qint64 lastPos;
    QString shortName, fileName, _tmpString, filter;
    QFile file;
    QFileSystemWatcher * watcher;
    TextWidget * textView;
    Highlight * highlightController;
    Highlighter * highlighter;
    QSettings settings;
    QPalette p, pausePalette;
    QTextCharFormat textFormat;
    QColor fontColor;
    QFont f;
    QComboBox *highlightSelector;
    QToolButton * pauseBtn;
    QPushButton * filterBtn;
    QStringList filterStrings;
};

#endif // TAILWIDGET_H
