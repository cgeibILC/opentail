#include "fileinfoframe.h"
#include "ui_fileinfoframe.h"

FileInfoFrame::FileInfoFrame(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::FileInfoFrame)
{
    ui->setupUi(this);
}

FileInfoFrame::~FileInfoFrame()
{
    delete ui;
}


void FileInfoFrame::setFileName(QString fileName)
{
    QString s;
    QFileInfo fi(fileName);
    ui->fileName->setText(fileName);
    s.setNum(fi.size());
    ui->fileSize->setText(s);
    ui->lastModified->setText(fi.lastModified().toString());
}
