#include "highlighter.h"
#include <QTextCharFormat>

Highlighter::Highlighter(QTextCharFormat normalTextFormat, QTextDocument *document, Highlight *highlightControllerGiven)
    :QSyntaxHighlighter(document)
{
    setFormat(0, 0, normalTextFormat);
    normalFormat = normalTextFormat;
    highlightController = highlightControllerGiven;
    groupName = highlightControllerGiven->getGroup();
}


void Highlighter::selectGroup(QString newGroupName)
{
    if (newGroupName.length()>0) {
        groupName = newGroupName;
    }else{
        newGroupName = groupName;
    }
    highlightController->setGroup(newGroupName);
    rehighlight();
}


void Highlighter::highlightBlock(const QString &text)
{
    enum { NormalState = -1, Highlighted };

    int state = previousBlockState();    
    int length = 0;
    bool found = false;
    QString stringFound;

    QTextCharFormat hightlightFormat;
    HighlightParameters currentHighlight;

    for (int i = 0; i < text.length(); i++) {        
        for (int highlightCounter=0;highlightCounter<highlightController->getHighlights().length();highlightCounter++){
            currentHighlight = highlightController->getHighlights().at(highlightCounter);
            length = currentHighlight.searchFor.length();
            stringFound = text.mid(i, length);
            if ( stringFound.compare(
                        currentHighlight.searchFor,
                                            (currentHighlight.ignoreCase ? Qt::CaseInsensitive :  Qt::CaseSensitive)
                                            ) == 0 )
            {
                hightlightFormat.setForeground(QColor(currentHighlight.fontColor));
                hightlightFormat.setBackground(QColor(currentHighlight.backgroundColor));
                hightlightFormat.setFontWeight(currentHighlight.bold ? QFont::Bold : QFont::Normal);
                state = Highlighted;
                if (currentHighlight.tray == true ){
                    emit foundString(currentHighlight.searchFor);
                }
                if (currentHighlight.wholeLine == true){
                    setFormat(0, text.length(), hightlightFormat);
                    found = true;
                    break;
                }else{
                    setFormat(i, i+length, hightlightFormat);
                    i = i + length;
                }
            }
        }

        if (state == Highlighted && found == false) {
            setFormat(i, text.length(), normalFormat);
            state = NormalState;
        }
    }

    setCurrentBlockState(state);
}
