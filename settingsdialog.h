#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QSettings>
#include <QColorDialog>
#include <QFontDialog>
#include <QFileDialog>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

public slots:
    void accept();
    void fontColorClicked();
    void fontSelector();
    void chooseShellExec();
    void backgroundColorClicked();
    void updateExample();

    void terminalFontSelector();


private:
    Ui::SettingsDialog *ui;
    QSettings settings;
    QPalette p;
    QColorDialog colorDialog;
};

#endif // SETTINGSDIALOG_H
