#ifndef NEWHIGHLIGHT_H
#define NEWHIGHLIGHT_H

#include <QDialog>

namespace Ui {
class NewHighlight;
}

class NewHighlight : public QDialog
{
    Q_OBJECT

public:
    explicit NewHighlight(QWidget *parent = 0);
    ~NewHighlight();
    QString getHighlightText();
    void setType(int);

private:
    Ui::NewHighlight *ui;
};

#endif // NEWHIGHLIGHT_H
