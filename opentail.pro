#-------------------------------------------------
#
# Project created by OpenTail 2013-10-02T16:19:59
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET =  opentail
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    tailwidget.cpp \
    highlighteditor.cpp \
    highlight.cpp \
    traylauncher.cpp \
    settingsdialog.cpp \
    textwidget.cpp \
    highlighter.cpp \
    newhighlight.cpp \
    filelist.cpp \
    QRecentFilesMenu.cpp \
    smtp.cpp \
    fileinfoframe.cpp

HEADERS  += mainwindow.h \
    tailwidget.h \
    highlighteditor.h \
    highlight.h \
    HighlightParameters.h \
    traylauncher.h \
    settingsdialog.h \
    textwidget.h \
    highlighter.h \
    newhighlight.h \
    filelist.h \
    QRecentFilesMenu.h \
    smtp.h \
    fileinfoframe.h

FORMS    += mainwindow.ui \
    highlighteditor.ui \
    settingsdialog.ui \
    newhighlight.ui \
    fileinfoframe.ui

RESOURCES += \
    resource/Icons.qrc \
    resource/TextFiles.qrc \
    translations.qrc

OTHER_FILES += \
    AUTHORS \
    COPYING.LIB \
    resource/opentail.desktop \
    translations/opentail_cs.ts \
    translations/opentail_da_DK.ts

macx {
     QMAKE_INFO_PLIST = Info.plist
     ICON = resource/logo.icns
}

 TRANSLATIONS += \
   translations/opentail_da_DK.ts \
    translations/opentail_cs.ts

unix {
  #VARIABLES
  isEmpty(PREFIX) {
    PREFIX = /usr
  }
  BINDIR = $$PREFIX/bin
  DATADIR =$$PREFIX/share

  DEFINES += DATADIR=\\\"$$DATADIR\\\" PKGDATADIR=\\\"$$PKGDATADIR\\\"

  #MAKE INSTALL

  INSTALLS += target desktop icon

  target.path =$$BINDIR

  desktop.path = $$DATADIR/applications
  desktop.files += $${TARGET}.desktop

  icon.path = $$DATADIR/icons/hicolor/scalable/apps
  icon.files += $${TARGET}.svg
}
