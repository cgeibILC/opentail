#include "textwidget.h"
#include <QMenu>
#include <QScrollBar>

TextWidget::TextWidget(QWidget *parent) :
    QPlainTextEdit(parent)
{
    ensureCursorVisible();
    markAsClicked = false;
}

void TextWidget::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu *menu = createStandardContextMenu();
    menu->addAction(tr("Clear"),this, SLOT(clear()));
    menu->exec(event->globalPos());
    delete menu;
}

void TextWidget::mousePressEvent(QMouseEvent *e)
{
    if (e->button()==Qt::RightButton){

    }

    markAsClicked = true;    
    emit(windowClicked());
}

void TextWidget::paintEvent(QPaintEvent * e)
{
    markAsClicked = false;
   /* if (markAsClicked){
        rect = e->rect();
        int x80 = document()->lineCount();
        lines << x80;

    }
    for (int i=0;i< lines.count();i++){
        qDebug("Draw at: %i", lines.at(i));
        QPainter p(this);
        p.setPen(QColor("red"));
        p.drawLine( 0, 0, 300, lines.at(i)/5);
    }*/
    QPlainTextEdit::paintEvent(e);
}

void TextWidget::resizeEvent ( QResizeEvent * event )
{
    QScrollBar *sb = verticalScrollBar();
    sb->setValue(sb->maximum());
    QPlainTextEdit::resizeEvent(event);
}

void TextWidget::search(QString searchFor)
{    

    int start = 0, begin = 0;
    int end = 0;
    while ( (begin = document()->find(searchFor, begin).position()) != -1) {
        start = begin-searchFor.length();
        end = start+searchFor.length();
        QTextCharFormat fmt;
        fmt.setBackground(Qt::yellow);
        fmt.setForeground(Qt::black);

        QTextCursor cursor(document());
        cursor.setPosition(start, QTextCursor::MoveAnchor);
        cursor.setPosition(end, QTextCursor::KeepAnchor);
        cursor.setCharFormat(fmt);
    }
}


