#include "filelist.h"

FileList::FileList(QWidget *parent) :
    QListWidget(parent)
{

    setAcceptDrops(true);
}

void FileList::dropEvent(QDropEvent *event)
{
    const QMimeData *mimeData = event->mimeData();
    if (mimeData->hasUrls()) {
        QList<QUrl> urlList = mimeData->urls();        
        for (int i = 0; i < urlList.size() && i < 32; ++i) {
            QString url = urlList.at(i).toLocalFile();
            emit openFile(url);
        }
    }
}

QString FileList::selectFile(const QString fileName)
{
    /*foreach(QListWidgetItem *item, items){
        if(item->text().compare(fileName)==0){
            return item->text();
        }
    }*/
    QList<QListWidgetItem*> items = findItems(fileName, Qt::MatchWrap );
    if (items.count()>0){
        setCurrentItem(items.at(0));
        return items.at(0)->text();
    }
    return NULL;

}

void FileList::dragEnterEvent ( QDragEnterEvent * event )
{
    event->accept();
}

void FileList::dragMoveEvent(QDragMoveEvent *event)
{
    event->accept();
}
