#include "traylauncher.h"

TrayLauncher::TrayLauncher(QMainWindow *parent)

{
    setWindowIcon(QIcon(":/tailerpng.svg"));
    parentWindow = parent;
    createActions();
    createTrayIcon();    
}


void TrayLauncher::createTrayIcon()
{    
    trayMenu = new QMenu(parentWindow);
    trayMenu->addAction(minimizeAction);
    trayMenu->addAction(maximizeAction);
    trayMenu->addAction(restoreAction);
    trayMenu->addSeparator();
    trayMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(parentWindow);
    trayIcon->setContextMenu(trayMenu);
    trayIcon->show();


    trayIcon->setIcon(windowIcon());
    trayIcon->setToolTip(tr("Click to open window"));
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(iconClicked(QSystemTrayIcon::ActivationReason)));
}

void TrayLauncher::createActions()
{
    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, SIGNAL(triggered()), parentWindow, SLOT(hide()));

    maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, SIGNAL(triggered()), parentWindow, SLOT(showMaximized()));

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, SIGNAL(triggered()), parentWindow, SLOT(showNormal()));

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, SIGNAL(triggered()), parentWindow, SLOT(closeApp()));
}

void TrayLauncher::alertFound(QString fileName, QString alertMessage)
{
    trayIcon->showMessage(alertMessage, fileName);    
}

void TrayLauncher::iconClicked(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::DoubleClick){
        if (parentWindow->isHidden()){
            parentWindow->show();
        }else{
            parentWindow->hide();
        }
    }
}
